#/bin/bash

a=1
for i in *.extension; do #replace .extension with your extension (gif, png, jpg, jpeg), same with next line 
  new=$(printf "%04d.extension" "$a")
  mv -i -- "$i" "$new"
  let a=a+1
done

# source: https://stackoverflow.com/a/3211670/16417115
